import tensorflow as tf
from tensorflow.python.ops import rnn, rnn_cell

from base.base_model import BaseModel


class LSTMModel(BaseModel):
    def __init__(self, config):
        super(LSTMModel, self).__init__(config)

        self.build_model()
        self.init_saver()

    def build_model(self):
        self.hm_epochs = 3
        self.n_classes = 10
        self.batch_size = 128
        self.chunk_size = 28
        self.save = 28
        self.rnn_size = 128
        self.x = tf.placeholder('float', [None, n_chunks, chunk_size])
        self.y = tf.placeholder('float')



    def init_saver(self):
        # here you initialize the tensorflow saver that will be used in saving the checkpoints.
        self.saver = tf.train.Saver(max_to_keep=self.config.max_to_keep)

def recurrent_neural_network(x):
    layer = {'weights':tf.Variable(tf.random_normal([rnn_size,n_classes])),
             'biases':tf.Variable(tf.random_normal([n_classes]))}

    x = tf.transpose(x, [1,0,2])
    x = tf.reshape(x, [-1, chunk_size])
    x = tf.split(x, n_chunks, 0)

    lstm_cell = rnn_cell.BasicLSTMCell(rnn_size,state_is_tuple=True)
    outputs, states = rnn.static_rnn(lstm_cell, x, dtype=tf.float32)

    output = tf.matmul(outputs[-1],layer['weights']) + layer['biases']

    return output