import pandas as pd

with open('../data/output-comma-narrow.csv', 'rb') as file:
    dane = pd.read_csv(file, quotechar='"', skipinitialspace=True)
print(dane.columns.tolist())

# with open(, 'wb') as output:
dane.to_pickle('../data/dane.pkl')
